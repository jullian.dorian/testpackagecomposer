<?php

use DorianJullian\TestPackageComposer\Inspire;

require 'vendor/autoload.php';

$inspire = new Inspire;
echo $inspire->display();
echo $inspire->inspiration($argv[1]) . " " . $argv[2];