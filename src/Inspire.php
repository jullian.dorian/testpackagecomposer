<?php

namespace DorianJullian\TestPackageComposer;

class Inspire {
	
	private $inspirations = [
		'Nice work today ! You doing great.', 'You are sad ? Stop being sad, you\'re the great'
	];
	
	public function __construct() {
		
	}

    public function inspiration($index) {
        return $this->inspirations[$index];
    }
	
	public function display() {
		return $this->inspirations[rand(0, sizeof($this->inspirations)-1)];
	}
	
}